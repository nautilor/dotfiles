#!/bin/sh
xrandr --output DVI-I-1 --off --output DVI-D-1 --off --output HDMI-0 --mode 1920x1080 --pos 1919x0 --rotate normal --output DP-0 --mode 1920x1080 --pos 0x0 --rotate normal
xrandr --output DP-0 --mode 1920x1080 --rate 144.00
xrandr --output HDMI-0 --mode 1920x1080 --rate 144.00
