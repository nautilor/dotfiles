:set tabstop     =3
:set softtabstop =4
:set shiftwidth  =4
:set expandtab

" VIM PLUG
call plug#begin('~/.vim/plugged')
    Plug 'lambdalisue/fern.vim',
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'frazrepo/vim-rainbow'
    Plug 'preservim/nerdcommenter'
call plug#end()

"VIM INTEGRATED
:noremap <C-j> :tabprevious<CR>
:noremap <C-k> :tabnext<CR>

" FERN DRAWER
:noremap <C-n> :Fern . -drawer -toggle<CR>

"FZF
" This is the default extra key bindings
:noremap <C-p> :FZF<CR>
let g:fzf_action = {
  \ 't': 'tab split',
  \ 'x': 'split',
  \ 'v': 'vsplit' }



filetype plugin indent on    " required
filetype plugin on

set updatetime=100

"git gutter
let g:gitgutter_async=0
let g:gitgutter_max_signs = 500
let g:gitgutter_map_keys = 0
let g:gitgutter_override_sign_column_highlight = 0
highlight clear SignColumn
highlight GitGutterAdd    guifg=#009900 ctermfg=2
highlight GitGutterChange guifg=#bbbb00 ctermfg=3
highlight GitGutterDelete guifg=#ff2222 ctermfg=1

"nerdcommenter
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDAltDelims_java = 1
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1

"vim-rainbow
let g:rainbow_active = 1

let g:rainbow_load_separately = [
    \ [ '*' , [['(', ')'], ['\[', '\]'], ['{', '}']] ],
    \ [ '*.tex' , [['(', ')'], ['\[', '\]']] ],
    \ [ '*.cpp' , [['(', ')'], ['\[', '\]'], ['{', '}']] ],
    \ [ '*.{html,htm}' , [['(', ')'], ['\[', '\]'], ['{', '}'], ['<\a[^>]*>', '</[^>]*>']] ],
    \ ]

let g:rainbow_guifgs = ['RoyalBlue3', 'DarkOrange3', 'DarkOrchid3', 'FireBrick']
let g:rainbow_ctermfgs = ['lightblue', 'lightgreen', 'yellow', 'red', 'magenta']
autocmd VimEnter * RainbowToggle
autocmd VimEnter * nested RainbowToggle

syntax on
"netrw config
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25



"function to toggle netrw
let g:NetrwIsOpen=0
function! ToggleNetrw()
    if g:NetrwIsOpen
        let i = bufnr("$")
        while (i >= 1)
            if (getbufvar(i, "&filetype") == "netrw")
                silent exe "bwipeout " . i
            endif
            let i-=1
        endwhile
        let g:NetrwIsOpen=0
    else
        let g:NetrwIsOpen=1
        silent Lexplore
    endif
endfunction

"toggle netrw with C-N
noremap <silent> <C-N> :call ToggleNetrw()<CR>

"open netrw if no file is provided
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | :call ToggleNetrw() | endif

