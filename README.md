# nautilor's dotfiles

These are my dotfiles

# Setup

- Distro: Arch Linux
- WM: BSPWM
- Menu: Rofi
- Terminal: rxvt-unicode
- Music Daemon: mopidy-spotify
- Music Player: ncmpcpp
- File Manager: PcManFM
- Coloscheme: Custom
- GTK Theme: Custom
- Icon Theme: Tela Purple Dark
- Font: Iosevka/Fantasque Sans Mono/Material Icons

> Rofi Configuration is done all in scripts
> I need to create a config file for it

# Screenshots
![Desktop](./screenshots/desktop.png)

![Terminal](./screenshots/terminal.png)

![File Manager](./screenshots/file_manager.png)

![System Menu](./screenshots/system_menu.png)

![Rofi Menu](./screenshots/rofi_menu.png)

![Rofi Launcher](./screenshots/rofi_launcher.png)

![Lockscreen](./screenshots/lockscreen.png)